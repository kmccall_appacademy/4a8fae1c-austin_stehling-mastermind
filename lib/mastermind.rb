class Code
  attr_reader :pegs
  PEGS = {"R" => :Red,
          "B" => :Blue,
          "G" => :Green,
          "Y" => :Yellow,
          "O" => :Orange,
          "P" => :Purple}

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(input)
    tester = PEGS.keys
    result = input.upcase.each_char.all? do |el|
      tester.include?(el)
    end
    if result == true
      Code.new(input.split(""))
    else
      raise "error"
    end
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.keys.sample}
    Code.new(pegs)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(guess)
    correct_guess = 0
    guess.pegs.each_index do |idx|
      correct_guess += 1 if self[idx] == guess[idx]
    end
    correct_guess
  end

  def near_matches(guess)
    exact = exact_matches(guess)
    counts = count

    guess.pegs.each do |el|
      counts[el] -= 1 if counts[el] > 0
    end
    count.values.inject(&:+) - (counts.values.inject(&:+) + exact_matches(guess))
  end

  def count
    count = Hash.new(0)
    self.pegs.each {|el| count[el] += 1}
    count
  end

  def ==(guess)
    return false if guess.is_a?(Code) == false
    self.pegs.map(&:downcase) == guess.pegs.map(&:downcase)
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Guess the code:"
    guess = gets.chomp
    begin
      Code.parse(guess)
    rescue
      puts "Error parsing code!"
      retry
    end
  end

  def display_matches(input)
    print "You have #{@secret_code.exact_matches(input)} exact matches."
    print "You have #{@secret_code.near_matches(input)} near matches."
  end
end
